
## GIT 
https://bitbucket.org/ev45ive/altkom-credit-suisse-tdd/src
Login
Klikamy (+) -> Fork this repository

git clone https://bitbucket.org/<TWOJ-LOGIN>/altkom-credit-suisse-tdd.git altkom-credit-suisse-tdd
cd altkom-credit-suisse-tdd

git remote add trener https://bitbucket.org/ev45ive/altkom-credit-suisse-tdd/

## Update
git pull trener master

git branch --set-upstream-to trener/master
git pull 


## Instalacje

node -v 
v16.13.1

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.64.2
f80445acd5a3dadef24aa209168452a3d97cc326

chrome://version
Google Chrome	98.0.4758.102 

## TypeScript

npm i -g typescript@latest

tsc --version
Version 4.6.2

tsc index.ts
tsc --watch index.ts

## Typescript tsconfig.json
tsc --init --outDir dist

Created a new tsconfig.json with:                                                                                       
TS
  target: es2016
  module: commonjs
  outDir: dist
  strict: true
  esModuleInterop: true
  skipLibCheck: true
  forceConsistentCasingInFileNames: true
You can learn more at https://aka.ms/tsconfig.json

<!-- uzywamy bez parametrow - czyta plik tsconfig.json -->
tsc 

## NPM 

npm init 
npm init -y

npm install --save-dev jest

echo 'node_modules' > .gitignore

https://semver.org/
https://semver.npmjs.com/

npm outdated

npm i // update to semver
npm ci

## Analiza Statyczna
- Compiles
  - Typescript 
- Style Checker
  - Eslint
  - https://eslint.org/docs/user-guide/getting-started
  - https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
- Formatter
  - Prettier ( + EsLint + TypeScript config)
  - https://prettier.io/docs/en/integrating-with-linters.html


# Jest + TypeScript

https://github.com/kulshekhar/ts-jest
npm i -D ts-jest typescript
npx ts-jest config:init

Unable to load the module "typescript". Using "ts-jest" requires this package to be installed. To fix it:
        ↳ install "typescript": `npm i -D typescript` (or `yarn add --dev typescript`) 

# Jest + GIT
No tests found related to files changed since last commit.
Press `a` to run all tests, or run Jest with `--watchAll`.


## Coverage
jest --coverage

./altkom-credit-suisse-tdd/trener/simple-tdd/coverage/lcov-report/maths.ts.html#L18

https://jestjs.io/docs/cli#--reporters
https://plugins.jenkins.io/junit/

## Jest Watch + InBand
inband - runs all tests in one process (faster for watch)

"test:dev": "jest --watch --runInBand",

npm run test:dev
https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script


## NextJS
https://nextjs.org/docs/testing

npx create-next-app@latest next-app --ts

npm install --save-dev jest @testing-library/react @testing-library/jest-dom

https://github.com/testing-library/jest-dom#custom-matchers

https://github.com/jsdom/jsdom


## Storybook
https://storybook.js.org/blog/get-started-with-storybook-and-next-js/

https://dev.to/jonasmerlin/how-to-use-the-next-js-image-component-in-storybook-1415#:~:text=the%20following%20to-,Storybook%27s%20setup%20code,-%3A


npx sb init --builder webpack5

npm i webpack@^5

Skipping the eslintPlugin fix.

If you change your mind, run 'npx sb@next automigrate'

https://storybook.js.org/addons/storybook-addon-next

More info: https://github.com/storybookjs/eslint-plugin-s   ││   torybook#readme      


npm run storybook

## Mock service worker
https://storybook.js.org/addons/msw-storybook-addon/