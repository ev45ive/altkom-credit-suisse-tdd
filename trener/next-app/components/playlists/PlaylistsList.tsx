import { Playlist } from "../../interfaces/Playlist";

type Props = {
  playlists?: Playlist[] | undefined;
};

export const PlaylistsList = ({ playlists }: Props) => {
  if (!playlists?.length) return <div>No playlists</div>;

  return (
    <div>
      {/* .list-group>.list-group-item.list-group-item-action*3{$. Playlist $$} */}
      <div className="list-group">
        {playlists.map((playlist) => (
          <button key={playlist.id} className="list-group-item list-group-item-action">
            {playlist.name}
          </button>
        ))}
      </div>
    </div>
  );
};
