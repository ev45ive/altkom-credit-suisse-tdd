import { Playlist } from "../../interfaces/Playlist";

type Props = {
  playlist?: Playlist;
};

export const PlaylistDetails = ({ playlist }: Props) => {
  if (!playlist) {
    return <p>No playlist selected</p>;
  }

  return (
    <div>
      <dl>
        <dt>Name:</dt>
        <dd data-testid="playlist_name">{playlist.name}</dd>

        <dt>Public:</dt>
        <dd aria-label="playlist_public">
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd data-testid="playlist_description">{playlist.description}</dd>
      </dl>
      <button className="btn btn-info">Edit</button>
    </div>
  );
};
