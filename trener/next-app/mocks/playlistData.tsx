import { Playlist } from "../interfaces/Playlist";


export const playlistData: Playlist[] = [
    {
        id: "123",
        name: "Playlist 123",
        public: true,
        description: "Best playlist 123",
    },
    {
        id: "234",
        name: "Playlist 234",
        public: false,
        description: "Best playlist 234",
    },
    {
        id: "345",
        name: "Playlist 345",
        public: true,
        description: "Best playlist 345",
    },
];
