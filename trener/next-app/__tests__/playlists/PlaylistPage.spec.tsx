import {
  render,
  fireEvent,
  waitFor,
  screen,
  logRoles,
  waitForElementToBeRemoved,
} from "@testing-library/react";

import { fetchPlaylists } from "../../pages/playlists/fetchPlaylists";
import PlaylistsPage from "../../pages/playlists";
import { playlistData } from "../../mocks/playlistData";
import { QueryClient, QueryClientProvider } from "react-query";

jest.mock("../../pages/playlists/fetchPlaylists", () => ({
  fetchPlaylists: jest.fn(),
}));

const setup = () => {
  (fetchPlaylists as jest.Mock).mockResolvedValue(playlistData);

  const queryClient = new QueryClient();
  render(
    <QueryClientProvider client={queryClient}>
      <PlaylistsPage />
    </QueryClientProvider>
  );
}

describe("Playlists Page", () => {


  it("fetches data from API", async () => {
    setup();

    await waitForElementToBeRemoved(() => screen.queryByText("Loading"))

    
    screen.getAllByText(/Playlist 123/)
    // screen.debug()

  });
});
