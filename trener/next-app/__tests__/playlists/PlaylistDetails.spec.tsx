import { render, screen } from "@testing-library/react";

import { PlaylistDetails } from "../../components/playlists/PlaylistDetails";
import { Playlist } from "../../interfaces/Playlist";

describe("Playlist Details", () => {
  const playlistData: Playlist = {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "Best playlist 123",
  };
  
  const setup = ({ playlist }: { playlist?: Playlist }) => {
    render(<PlaylistDetails playlist={playlist} />);
  };

  it("should render without playlist data", () => {
    // render(<PlaylistDetails />);
    setup({ playlist: undefined });

    screen.getByText("No playlist selected");
  });

  it("should render playlist details", () => {
    setup({ playlist: playlistData });

    const name = screen.getByTestId("playlist_name");
    expect(name).toHaveTextContent(playlistData.name);

    const description = screen.getByTestId("playlist_description");
    expect(description).toHaveTextContent(playlistData.description);

    // logRoles(container);
  });

  it("should render Yes for public", () => {
    const playlist = { ...playlistData, public: true };
    setup({ playlist });

    const publicEl = screen.getByRole("definition", {
      name: "playlist_public",
    });
    expect(publicEl).toHaveTextContent("Yes");
  });

  it("should render No for private", () => {
    const playlist = { ...playlistData, public: false };
    setup({ playlist });

    const publicEl = screen.getByRole("definition", {
      name: "playlist_public",
    });
    expect(publicEl).toHaveTextContent("No");
  });
});
