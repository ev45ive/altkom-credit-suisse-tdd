import { render, screen } from "@testing-library/react";
import { PlaylistsList } from "../../components/playlists/PlaylistsList";
import { Playlist } from "../../interfaces/Playlist";
import { playlistData } from "../../mocks/playlistData";

describe("Playlists List", () => {
  const setup = ({ playlists }: { playlists?: Playlist[] }) => {
    render(<PlaylistsList playlists={playlists} />);
  };

  it("shows No playlists", () => {
    setup({ playlists: undefined });
    screen.getByText("No playlists");

    const items = screen.queryAllByText(/Playlist/);
    expect(items).toHaveLength(0);
  });

  it("shows list of playlists", () => {
    setup({ playlists: playlistData });

    const items = screen.queryAllByRole("button", {
      name: /Playlist/,
    });
    expect(items).toHaveLength(playlistData.length);
    for (let i in items) {
      expect(items[i]).toHaveTextContent(new RegExp(playlistData[i].name));
    }
  });
});
