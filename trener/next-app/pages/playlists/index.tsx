import { PlaylistDetails } from "../../components/playlists/PlaylistDetails";
import { PlaylistsList } from "../../components/playlists/PlaylistsList";
import { useQuery } from "react-query";
import { fetchPlaylists } from "./fetchPlaylists";

export default function PlaylistsPage(props: {}) {
  const { data, isLoading } = useQuery("playlists", fetchPlaylists);

  return (
    <div>
      {isLoading && <p>Loading</p>}
      {/* .row>.col*2 */}
      <div className="row">
        <div className="col">
          <h5 className="display-5">Playlists</h5>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <PlaylistsList playlists={data} />
        </div>
        <div className="col">
          <PlaylistDetails />
        </div>
      </div>
    </div>
  );
}
