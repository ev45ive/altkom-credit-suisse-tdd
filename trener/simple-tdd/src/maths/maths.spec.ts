import { add, divide, multiply, substract, _roundResult } from "./maths"

describe('Math module', () => {

    it('adding numbers', () => {
        expect(add(123, 321)).toBe(444)
        expect(add(200, -150)).toBe(50)
        expect(add(0, -150)).toBe(-150)
        // https://pl.wikipedia.org/wiki/IEEE_754 0.30000000000000004
        expect(add(0.1, 0.2)).toBe(0.3)
    })

    it('substract numbers', () => {
        expect(substract(2, 2)).toBe(0)
        expect(substract(0.4, 0.1)).toBe(0.3)
    })

    it('multiplies numbers', () => {
        expect(multiply(2, 2)).toBe(4)
        expect(multiply(3, 0.1)).toBe(0.3)
    })

    it('divide numbers', () => {
        expect(divide(2, 2)).toBe(1)
        expect(divide(3, 10)).toBe(0.3)
        expect(divide(3, 0.1)).toBe(30)
        // expect(() => divide(100, 0)).toThrowError('Cannot divide by 0')
    })

    it('rounds results', () => {
        expect(_roundResult(0.1 + 0.2)).toBe(0.3)
        expect(_roundResult(0.4 - 0.1)).toBe(0.3)
        expect(_roundResult(0.1 * 3)).toBe(0.3)
        expect(_roundResult(3 / 10)).toBe(0.3)
    })
})


test('test testing works', () => {

    expect(1 + 2).toBe(3)
    // expect({ x: 2 }).toBe({ x: 2 }) // shallow - Te same
    expect({ x: 2 }).toEqual({ x: 2 }) // deep - Wystarczajaco podobne (zawiera sie)
    expect({ x: 2 }).toStrictEqual({ x: 2 }) // deep - Dokładnie Takie Same
    expect(1).toBeTruthy()
    expect([1]).toContain(1)

    const result = {
        name: 'Alice',
        age: 21,
        pets: ['dog', 'snakes']
    }
    expect(result).toMatchObject({
        age: expect.any(Number),
        pets: expect.arrayContaining(['dog'])
    })

}) 
