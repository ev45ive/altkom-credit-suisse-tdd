

/**
 * Adding numbers
 *
 * @export
 * @param {number} x
 * @param {number} y
 * @return {*} 
 */
export function add(x: number, y: number) {
    const result = x + y
    return _roundResult(result)
}

export function substract(x: number, y: number) {
    const result = x - y
    return _roundResult(result)
}

export function multiply(x: number, y: number) {
    const result = x * y
    return _roundResult(result)
}

export function divide(x: number, y: number) {
    if (y === 0) {
        throw new Error('Cannot divide by 0')
    }
    const result = x / y
    return _roundResult(result)
}

export function _roundResult(result: number) {
    const parsed = result.toFixed(2)
    return parseFloat(parsed)
}

