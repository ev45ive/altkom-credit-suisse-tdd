import { FieldControl, IValidator, ValidationError } from "./interfaces"
import { FieldValidator, Validators } from "./FieldValidator"


describe('Validation module', () => {

    it('supports custom validators', () => {

        // const MockValidator1 = {
        //     validate(control: { value: any }): ValidationError | null {
        //         return { message: 'Mock error' }
        //     }
        // }
        // const spy1 = jest.spyOn(MockValidator1, 'validate')

        // Arrange:
        const MockValidator = {
            validate: jest.fn<ValidationError | null, [{ value: string }]>()
            // validate(): ValidationError | null { return null },
        }
        MockValidator.validate.mockReturnValue({ message: 'Mock error' })
        
        // Act:
        const validator = new FieldValidator([MockValidator])
        const result = validator.validate({ value: 'fake value' })
        
        // Assert:
        expect(MockValidator.validate).toHaveBeenCalledTimes(1)
        expect(MockValidator.validate).toHaveBeenCalledWith({ value: 'fake value' })
        expect(result).toHaveProperty('message', 'Mock error')
    })

    describe('shows error only:', () => {
        let emptyControl: FieldControl
        let textControl: FieldControl
        let shortControl: FieldControl
        let mediumControl: FieldControl
        let longControl: FieldControl

        beforeEach(() => {
            emptyControl = { value: '' }
            textControl = { value: 'some text' }
            shortControl = { value: '12' }
            mediumControl = { value: '123' }
            longControl = { value: '123 longer' }
        })

        it('when required field is empty', () => {
            // Arrange / given... 
            const validator = new FieldValidator([new Validators.Required()])
            const validationError: ValidationError = { message: 'Field is required' }

            // Act / when...
            const goodResult = validator.validate(textControl)
            const badResult = validator.validate(emptyControl)

            // Assert / then...
            expect(badResult).toEqual(validationError)
            expect(goodResult).toEqual(null)
        })
        it('when field value is required and has minlength', () => {

            const validator = new FieldValidator([
                new Validators.Required(),
                new FieldValidator([
                    new Validators.MinLength(3),
                ])
            ])
            expect(validator.validate({ value: '' })).toHaveProperty('message', 'Field is required')
            expect(validator.validate({ value: '12' })).toHaveProperty('message', 'Field value is too short')
        })

        it('when field value is not empty but too long', () => {
            // Arrange / given... 
            const validator = new FieldValidator([
                new Validators.MinLength(3)
            ])
            const validationError: ValidationError = { message: 'Field value is too short' }

            // Act / when...
            const emptyResult = validator.validate(emptyControl)
            const goodResult = validator.validate(longControl)
            const goodResultExact = validator.validate(mediumControl)
            const badResult = validator.validate(shortControl)

            // Assert / then...
            expect(emptyResult).toEqual(null)
            expect(badResult).toEqual(validationError)
            expect(goodResultExact).toEqual(null)
            expect(goodResult).toEqual(null)
        })

        it.todo('when field value is too short')
        it.todo('when checkbox is not checked')
        it.todo('when email is invalid')
    })
})