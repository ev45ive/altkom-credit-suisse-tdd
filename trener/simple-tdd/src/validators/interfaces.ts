

export interface FieldControl {
    value: string;
}
export interface ValidationError {
    message: string;
}

export interface IValidator {
    validate(control: FieldControl): ValidationError | null
}

