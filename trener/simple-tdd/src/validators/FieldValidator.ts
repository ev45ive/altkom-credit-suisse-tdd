import { FieldControl, IValidator, ValidationError } from "./interfaces";

// type Validator =
//     | { type: 'required' }
//     | { type: 'minlength', minlength: number };

export namespace Validators {
    export class Required implements IValidator {
        validate(control: FieldControl): ValidationError | null {
            if (control.value === '') return { message: 'Field is required' }
            return null
        }

    }
    export class MinLength implements IValidator {
        constructor(private minlength: number = 0) { }
        validate(control: FieldControl): ValidationError | null {
            if (control.value !== '' && control.value.length < this.minlength)
                return { message: 'Field value is too short' }
            return null
        }

    }
}

export class FieldValidator {

    constructor(private validators: IValidator[]) { }

    validate(control: FieldControl): ValidationError | null {
        for (let currentValidator of this.validators) {
            const result = currentValidator.validate(control)
            if (result !== null) {
                return result
            }
        }
        return null
    }
}
