// @ts-check

// @ts-nocheck

/**
 * @typedef Person
 * @property {number} id
 * @property {string} name
 * @property {number} [age]
 */

/** @type Person */
const p = { id: 123, name: 'test' }

/**
 * Adds numbers
 *
 * @param {number} x
 * @param {number} y
 * @returns {number}
 */
export function add(x, y) {
    return x + y;
}

const x = add(1, 2)

// TODO: Fix this;
// @ts-expect-error
const y = add('123', '123')

// @ts-ignore
const z = add('123', 123)

export { }