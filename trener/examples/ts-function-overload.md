```ts


// function add(x: number | string, y: number|string) {
function add(x: number, y: number): number // overloading
function add(x: string, y: string): string
function add(x: any, y: any): number | string { // type union
    // type narrowing / type guard
    if (typeof x === 'number' && typeof y === 'number') {
        return x + y;
    }
    if (typeof x === 'string' && typeof y === 'string') {
        return x + y;
    }
    throw new Error('Bad params')
}


add(1, 2)
add('ala ma ', 'kota')

let z = add('ala ma ', -2)

z.get.me.a.million.dollars()

```